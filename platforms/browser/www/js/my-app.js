// Initialize app
var myApp = new Framework7();


// If we need to use custom DOM library, let's save it to $$ variable:
var $$ = Dom7;

// Add view
var mainView = myApp.addView('.view-main', {
    // Because we want to use dynamic navbar, we need to enable it for this view:
    dynamicNavbar: true
});

// Handle Cordova Device Ready Event
$$(document).on('deviceready', function() {
    console.log("Device is ready!");

    var loggedIn = myApp.formGetData('loggedIn');
    console.log(loggedIn)
    if (loggedIn)
    {
        mainView.router.loadPage('product.html');
    }
    else
    {
        mainView.router.loadPage('login.html');
    }
});

var baseUrl = 'http://localhost/api';
var url = {
    login: baseUrl + '/login',
    products: {
        list: baseUrl + '/products',
        request: baseUrl + '/request_products',
    }
};

myApp.onPageInit('login', function(page) {
    $$('#login-button').click(function(e) {
        mainView.router.loadPage('product.html');
        // var email = $$('#email').val();
        // var password = $$('#password').val();
        // $$.post(url.login, {email: email, password: password}, function(resp) {
        //     console.log(resp);
        // });
    });
});

myApp.onPageInit('product', function(page) {
    var template = $$('script#productsTemplate').html();
    var compiledTemplate = Template7.compile(template);

    $$.getJSON(url.products.list, function(resp) {
        var html = compiledTemplate(resp.data);
        $$('#product-list').html(html);

        $$('.item-link').click(function(e) {
            var link = $$(this);
            var id = link.data('id');
            var title = link.data('title');

            myApp.formStoreData('id', id);
            myApp.formStoreData('title', title);

            mainView.router.loadPage('request.html');
        });
    });

    $$.getJSON(url.products.request, function(resp) {
        var data = [];
        $$.each(resp.data, function(i, item) {
            data.push({
                title: item.title,
                quantity: item.quantity,
                description: item.created_at
            });
        })
        var html = compiledTemplate(data);
        $$('#request-list').html(html);
    });
});


myApp.onPageInit('request', function(page) {
    var id = myApp.formGetData('id');
    var title = myApp.formGetData('title');
    $$('#product-title').html(title);

    $$('#request-button').click(function(e) {
        $$.post(url.products.request, {product_id: id, quantity: $$('#quantity').val()}, function(resp) {
            mainView.router.reloadPage('product.html');
            mainView.router.back();
        });
    });
});


// Now we need to run the code that will be executed only for About page.

// Option 1. Using page callback for page (for "about" page in this case) (recommended way):
myApp.onPageInit('about', function (page) {
    // Do something here for "about" page

})

// Option 2. Using one 'pageInit' event handler for all pages:
$$(document).on('pageInit', function (e) {
    // Get page data from event data
    var page = e.detail.page;

    if (page.name === 'about') {
        // Following code will be executed for page with data-page attribute equal to "about"
        myApp.alert('Here comes About page');
    }
})

// Option 2. Using live 'pageInit' event handlers for each page
$$(document).on('pageInit', '.page[data-page="about"]', function (e) {
    // Following code will be executed for page with data-page attribute equal to "about"
    myApp.alert('Here comes About page');
})